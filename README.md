# Artificial Intelligence @ Sapienza official Docker

## Introduzione
Le seguenti istruzioni permettono di istanziare un docker container: 
Il contenuto consiste in tool didattici, con il fine di affiancare la pratica alla teoria.

## Come iniziare
Queste istruzioni permetteranno di installare Docker, il container e di creare una shared folder fra l'host e i container.

### Prerequisiti
1. Una macchina con sistema operativo Unix o Windows.
2. Bisogna avere i permessi di ***root*** e avere disponibili la porta 8080 (Apache Webserver). (Le porte esposte dai container verso la macchina host possono essere modificate editando il file `.env`).
3. [Docker](https://www.docker.com/), docker-compose e git installati. Per la loro installazione, si seguano le istruzioni in base al proprio sistema operativo.
3.1 WINDOWS USER: Si consiglia l'uso di [git per SO windows](https://gitforwindows.org/) e della loro shell integrata per l'utilizzo di questa guida. 
4. UNIX USER: Si consiglia di leggere la sezione 'Abilitare l'uso di software con GUI' prima di proseguire con l'installazione del container.

### Installazione ed avviamento tramite docker-compose
1. Aprire una shell
2. Posizionarsi nella directory dove si vuole installare il tutto (ad es., `$HOME/ai/`). Da qui in poi, chiameremo questa directory `$BASE_DIR`.

        cd "$BASE_DIR"

3. Scaricare questo repository nella directory corrente. Per scoprire il comando da utilizzare:
    1. cliccare il pulsante "Clone" in alto in questa pagina; 
    2. scegliere il metodo "https" dal menu, copiare il comando mostrato (`git clone https://...`);
    3. incollare il comando nella shell aperta.

4. Avviare il docker container:

        docker-compose up -d 

Alla prima esecuzione, il comando scaricherà da Internet l'immagine e istanzierà il container. 

## Esecuzione dei test

Per controllare che tutto sia andato a buon fine: 
1. Puntare il proprio browser alla URL <http://localhost:8080> (ovviamente usare il numero di porta corretto se modificato nel file `.env`) ed assicurarsi che venga correttamente caricata la pagina di apache.
## Lista di comandi per l'avvio dei tool didattici
Eseguire il tool che più si preferisce nella shell e puntare la cartella /aiData contenente i vostri file da eseguire. 
`bgwalksat  cadical  eqsatz20  glucose  glucose-syrup  minisatAllModels	minisat_v1.14  minisat_v2.2.0  picosat	satz  zchaff`
`CspGeneticFramework LightHouse`
## Condividere file tra il Docker e il File System host
La sottodirectory `aiData` di `$BASE_DIR` è visibile all'interno del container docker.
Questa directory potrà essere usata per condividere i files da eseguire con i vari tool didattici.

## Utilizzare il Docker da linea di comando:
E' possibile avviare una shell nel container attraverso il seguente comando:

        docker exec -ti ai_sapienza bash
NB: Se utenti Windows aggiungere il prefisso winpty al comando precedente.

## Abilitare l'uso di software con GUI
Tutti questi passaggi bisogna eseguirli prima della creazione del container (docker-compose up -d)
Dopo aver eseguito i passaggi per il vostro sistema operativo potete testare con il seguente comando:

```sh
CspGeneticFramework /opt/demo/CSPGenetic/n-queens.txt
```

### Linux
Eseguire il seguente comando, se richiesto utilizzare con permessi da utente privileggiato.
```sh
xhost +
```

### MacOS X
Assicurarsi di aver scaricato e installato xQuartz
Eseguire il comando, e dirigersi sotto la tab Preferences -> Security, in Security abilitare il flag "Allow connection from network clients".
```sh
open -a XQuartz
```
Eseguire questi comandi
```sh
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip
DISPLAY=$ip:0
export DISPLAY
```
Per non eseguire manualmente questi passaggi, si consiglia di modificare il proprio file bashrc (o zshrc ecc)
## Altri utili comandi Docker

5. Per fermare l'esecuzione del container usare il comando:

        docker-compose stop

5. Per avviare l'esecuzione del container usare il comando:

        docker-compose start
    
5. Per reinstallare il docker container usare il comando:

        docker-compose down
        docker-compose up -d

I dati (ad es., il contenuto dei propri lavori) resteranno salvati nella cartella $BASE_DIR.

Si rinvia alla documentazione di Docker per gli usi più avanzati.

## Authors

* **Andrea Bacciu**  [Github profile](https://github.com/andreabac3)
* **Valerio Neri**   [Github profile](https://github.com/selektion)

## Riferimenti
* **Docker:** [docker.com](https://www.docker.com/) 